namespace TCP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Stops
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StopId { get; set; }

        [StringLength(101)]
        public string StopName { get; set; }
    }
}
